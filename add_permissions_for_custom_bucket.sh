aws lambda add-permission --function-name RayanUnitigsBatchLambdaInvokeFunction --principal s3.amazonaws.com \
    --statement-id s3invoke --action "lambda:InvokeFunction" \
    --source-arn arn:aws:s3:::serratus-rayan \
    --source-account $(aws sts get-caller-identity --query Account --output text)
