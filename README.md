# Construction of [unitigs/contigs] using [BCALM/Minia] on AWS Batch

### Source

Taken from: ["Orchestrating an Application Process with AWS Batch using AWS CloudFormation"](https://aws.amazon.com/blogs/compute/orchestrating-an-application-process-with-aws-batch-using-aws-cloudformation/)

But I removed all the CodeCommit stuff (replaced by `deploy-docker.sh` manual deployment to ECR).

Amazon Elastic Container Registry (ECR) is used as the Docker container registry. AWS Batch will be triggered by the lambda when a dataset file is dropped into the S3 bucket. 

Provided CloudFormation template has all the services (VPC, Batch *managed*, IAM roles, EC2 env, S3, Lambda)

### Installation 

Execute the below commands to spin up the infrastructure cloudformation stack.

```
./deploy-docker.sh [-assembly]
./create_tools_bucket.sh 
./spinup.sh
```

Use the `-assembly` flag to deploy the `fastp+Miniak31` Dockerfile instead of BCALM.

### Testing

1. AWS S3 bucket - batch-unitigs-<YOUR_ACCOUNT_NUMBER> is created as part of the stack.
2. Drop a dataset in it. This will trigger the Lambda to trigger the AWS Batch
3. In AWS Console > Batch, Notice the Job runs and performs the operation based on the pushed container image.

### Code Cleanup

In short:

```
./cleanup.sh
```

Which does:

1. AWS Console > S3 bucket - batch-unitigs-<YOUR_ACCOUNT_NUMBER> - Delete the contents of the file
3. run the below command to delete the stack.

    ```
    $ aws cloudformation delete-stack --stack-name batch-processing-job

    ```

What it doesn't do:

3. AWS Console > ECR - batch-processing-job-repository - delete the image(s) that are pushed to the repository

## License

This library is licensed under the MIT-0 License. See the LICENSE file.

