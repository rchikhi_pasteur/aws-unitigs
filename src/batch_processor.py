import boto3
from boto3.dynamodb.conditions import Key, Attr
import csv, sys, time, argparse
from datetime import datetime
import json
import os
import sys
from operator import itemgetter, attrgetter
from time import sleep
import urllib3
import json

LOGTYPE_ERROR = 'ERROR'
LOGTYPE_INFO = 'INFO'
LOGTYPE_DEBUG = 'DEBUG'

def process_file(inputBucket, fileName, region, delete_original=False):
    #try:
    if True:
        urllib3.disable_warnings()
        s3 = boto3.client('s3')
        
        prefix = fileName
        print("region - " + region)
        startTime = datetime.now()

        # go to /tmp (important, that's where local storage / nvme is)
        os.chdir("/tmp")
        
        # check free space
        os.system(' '.join(["df", "-h", "."]))

        # download reads from s3
        local_file = os.path.basename(str(fileName))
        s3.download_file(inputBucket, fileName, local_file)
        print("downloaded file to",local_file)

        # run bcalm
        os.system(' '.join(["../bcalm", "-kmer-size", "31", "-in", local_file, "-skip-links"]))
        
        unitigs_filename = '.'.join(local_file.split('.')[:-1])+".unitigs.fa"

        # run mfc 
        os.system(' '.join(["../MFCompressC",unitigs_filename]))
        
        compressed_unitigs_filename = unitigs_filename + ".mfc"
        
        # upload unitigs to s3
        outputBucket = "serratus-rayan" 
        outputFolder = "unitigs/"
        s3.upload_file(compressed_unitigs_filename, outputBucket, outputFolder + compressed_unitigs_filename)

        # delete original file, maybe
        if delete_original:
            logMessage(fileName, "Deleting original file", LOGTYPE_INFO) 
            s3.delete_object(Bucket = inputBucket, Key = fileName)

        endTime = datetime.now()
        diffTime = endTime - startTime
        logMessage(fileName, "File processing time - " + str(diffTime.seconds), LOGTYPE_INFO) 

    #except Exception as ex:
    #    logMessage(fileName, "Error processing files:" + str(ex), LOGTYPE_ERROR)  

def main():
    inputBucket = ""
    fileName = ""
    region = "us-east-1"
    delete_original = False
   
    if "InputBucket" in os.environ:
        inputBucket = os.environ.get("InputBucket")
    if "FileName" in os.environ:
        fileName = os.environ.get("FileName")
    if "Region" in os.environ:
        region = os.environ.get("Region")
    if "DeleteOriginal" in os.environ:
        delete_original = True

    try:          
        if inputBucket == "" or fileName == "":
            parser = argparse.ArgumentParser()
            parser.add_argument("--bucketName", "-b", type=str, required=True)
            parser.add_argument("--fileName", "-f", type=str, required=True)
            parser.add_argument("--region", "-r", type=str, required=True)
            parser.add_argument("--delete-original", "-d", dest='delete_original', action='store_true')
            args = parser.parse_args()

            inputBucket = args.bucketName
            fileName = args.fileName
            region = args.region

            delete_original = args.delete_original

    except Exception as ex:
        logMessage(fileName, "Unexpected error during arg parsing (due to lack of environment variables):" + str(ex), LOGTYPE_ERROR)
        
    logMessage(fileName, 'parameters: ' + inputBucket + "  " + fileName +  "  " + region, LOGTYPE_INFO)

    process_file(inputBucket, fileName, region, delete_original)


def logMessage(fileName, message, logType):
    try:
        logMessageDetails = constructMessageFormat(fileName, message, "", logType)
        
        if logType == "INFO" or logType == "ERROR":
            print(logMessageDetails)
        elif logType == "DEBUG":
            try:
                if os.environ.get('DEBUG') == "LOGTYPE":
                   print(logMessageDetails) 
            except KeyError:
                pass
    except Exception as ex:
        logMessageDetails = constructMessageFormat(fileName, message, "Error occurred at Batch_processor.logMessage" + str(ex), logType)
        print(logMessageDetails)


def constructMessageFormat(fileName, message, additionalErrorDetails, logType):
    if additionalErrorDetails != "":
        return "fileName: " + fileName + " " + logType + ": " + message + " Additional Details -  " + additionalErrorDetails
    else:
        return "fileName: " + fileName + " " + logType + ": " + message

if __name__ == '__main__':
   main()
