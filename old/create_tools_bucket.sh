aws s3api create-bucket --acl public-read --bucket aws-unitigs-tools
aws s3 cp aws-unitigs-tools/bcalm-binaries-v2.2.3-Linux.tar.gz s3://aws-unitigs-tools/
aws s3 cp aws-unitigs-tools/MFCompress-linux64-1.01.tgz s3://aws-unitigs-tools/

aws s3api put-object-acl --bucket aws-unitigs-tools --key bcalm-binaries-v2.2.3-Linux.tar.gz --acl public-read
aws s3api put-object-acl --bucket aws-unitigs-tools --key MFCompress-linux64-1.01.tgz --acl public-read
