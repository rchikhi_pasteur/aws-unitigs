ID=$(aws sts get-caller-identity --query Account --output text)
aws s3 rm s3://batch-unitigs-$ID --recursive 
sleep 5
aws cloudformation delete-stack --stack-name batch-unitigs
